#pragma once
#include <iostream>
#include <stdlib.h>
#include <Windows.h>
#include <gl\glut.h>
#include <gl\glext.h>

using namespace std;


class FileManager
{
public:
	FileManager(){};
	~FileManager(){};


public:
	int	m_bitmapIdx = 1;


public:
	void ScreenCapture(int wid, int hei)
	{
		string zeros, fullname;

		if (m_bitmapIdx< 10)
			zeros = "00000";
		else if (m_bitmapIdx< 100)
			zeros = "0000";
		else if (m_bitmapIdx< 1000)
			zeros = "000";
		else if (m_bitmapIdx< 10000)
			zeros = "00";
		else if (m_bitmapIdx< 100000)
			zeros = "0";

		fullname = "Scene/" + zeros + to_string(m_bitmapIdx) + ".bmp";

		char filename[255];
		sprintf_s(filename, 255, fullname.c_str(), m_bitmapIdx);

		BITMAPFILEHEADER bf;
		BITMAPINFOHEADER bi;

		unsigned char *image = (unsigned char*)malloc(sizeof(unsigned char)*wid*hei * 3);
		FILE *file;
		fopen_s(&file, filename, "wb");
		if (image != NULL)
		{
			if (file != NULL)
			{
				glReadPixels(0, 0, wid, hei, GL_BGR_EXT, GL_UNSIGNED_BYTE, image);

				memset(&bf, 0, sizeof(bf));
				memset(&bi, 0, sizeof(bi));

				bf.bfType = 'MB';
				bf.bfSize = sizeof(bf) + sizeof(bi) + wid*hei* 3;
				bf.bfOffBits = sizeof(bf) + sizeof(bi);
				bi.biSize = sizeof(bi);
				bi.biWidth = wid;
				bi.biHeight = hei;
				bi.biPlanes = 1;
				bi.biBitCount = 24;
				bi.biSizeImage = wid*hei * 3;

				fwrite(&bf, sizeof(bf), 1, file);
				fwrite(&bi, sizeof(bi), 1, file);
				fwrite(image, sizeof(unsigned char), hei*wid * 3, file);

				fclose(file);
			}
			free(image);
		}

		m_bitmapIdx++;
	}
};

