#pragma once
#include <string>

using namespace std;

class Point;
class Mesh;
class Spring
{
public:
	Spring(){};
	~Spring(){};

public:

	int pointIDX[2];

	Point	*p1, *p2;
	Mesh	*m1, *m2;
	float	initial_length;
	float	length;
	float	stiffness;
	string	type;


public:
	bool inSpring(Point* p)
	{
		if (p1 == p || p2 == p)
			return true;
		else
			return false;
	}
};

