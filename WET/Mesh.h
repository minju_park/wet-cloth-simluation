#ifndef _Mesh_H_
#define _Mesh_H_

#include "Vector.h"
#include "Spring.h"
#include "Point.h"
#include <vector>
#include <algorithm>

class Spring;
class Point;
class Mesh
{
public:
	Mesh(){};
	~Mesh(){};

	int idx[2];
	int indx;
	int pointIDX[3];

	Point	*p1, *p2, *p3;
	Spring	*s1, *s2, *s3;
	Vector	normal;
	
	Vector BBmin, BBmax;
	
	float area;

	bool contact = false;

	
public:
	void BB()
	{
		Vector minBB = { +999999999.0f, +999999999.0f, +999999999.0f };
		Vector maxBB = { -999999999.0f, -999999999.0f, -999999999.0f };

		minBB.x = min(minBB.x, p1->position.x);
		minBB.x = min(minBB.x, p2->position.x);
		minBB.x = min(minBB.x, p3->position.x);

		maxBB.x = max(maxBB.x, p1->position.x);
		maxBB.x = max(maxBB.x, p2->position.x);
		maxBB.x = max(maxBB.x, p3->position.x);

		minBB.y = min(minBB.y, p1->position.y);
		minBB.y = min(minBB.y, p2->position.y);
		minBB.y = min(minBB.y, p3->position.y);

		maxBB.y = max(maxBB.y, p1->position.y);
		maxBB.y = max(maxBB.y, p2->position.y);
		maxBB.y = max(maxBB.y, p3->position.y);

		minBB.z = min(minBB.z, p1->position.z);
		minBB.z = min(minBB.z, p2->position.z);
		minBB.z = min(minBB.z, p3->position.z);

		maxBB.z = max(maxBB.z, p1->position.z);
		maxBB.z = max(maxBB.z, p2->position.z);
		maxBB.z = max(maxBB.z, p3->position.z);

		BBmin = minBB;
		BBmax = maxBB;
	}

	bool inMesh(Point *p)
	{
		if (p1->idx == p->idx || p2->idx == p->idx || p3->idx == p->idx)
			return true;
		else
			return false;
	}

	bool inMesh(Spring *s)
	{
		if (((s1->p1->idx == s->p1->idx && s1->p2->idx == s->p2->idx) || (s1->p2->idx == s->p1->idx && s1->p1->idx == s->p2->idx))
			|| ((s2->p1->idx == s->p1->idx && s2->p2->idx == s->p2->idx) || (s2->p2->idx == s->p1->idx && s2->p1->idx == s->p2->idx))
			|| ((s3->p1->idx == s->p1->idx && s3->p2->idx == s->p2->idx) || (s3->p2->idx == s->p1->idx && s3->p1->idx == s->p2->idx)))
			return true;
		else
			return false;
	}
};

#endif