#pragma once
#include "Vector.h"
#include "Point.h"
#include "Spring.h"
#include "Mesh.h"
#include <vector>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <string>

using namespace std;

class MeshLoader
{
public:
	MeshLoader(){};
	~MeshLoader(){};

public:
	std::vector<Point> vertics;
	std::vector<Spring> edges;
	std::vector<Mesh> faces;

	Vector verticsMax = { -999999999.0f, -999999999.0f, -999999999.0f };
	Vector verticsMin = { +999999999.0f, +999999999.0f, +999999999.0f };
	Vector verticsCom = { 0.0f, 0.0f, 0.0f };

	void LoadMesh(string filename, float scale, float translateX, float translateY, float translateZ)
	{
		vertics.clear();
		faces.clear();
		edges.clear();

		fstream fp(filename, ios::in);

		string str;

		if (fp.fail())
		{
			cerr << "[ERROR] Check your file name! >>>> There is no " << filename << endl;
			return;
		}

		while (!fp.eof())
		{
			fp >> str;

			if (str == "v" || str == "vf")
			{
				float a, b, c;
				fp >> a >> b >> c;

				Point v;

				v.acceleration = { 0, 0, 0 };

				v.pre_velo = { 0, 0, 0 };
				v.velocity = { 0, 0, 0 };

				v.position.x = scale*a + translateX;
				v.position.y = scale*b + translateY;
				v.position.z = scale*c + translateZ;

				v.pre_pos = v.position;
				v.prepre_pos = v.position;

				v.normal = { 0, 0, 0 };

				if (str == "vf") v.fixed = true;
		
				vertics.push_back(v);

				verticsMin.x = min(verticsMin.x, v.position.x);
				verticsMax.x = max(verticsMax.x, v.position.x);
				verticsMin.y = min(verticsMin.y, v.position.y);
				verticsMax.y = max(verticsMax.y, v.position.y);
				verticsMin.z = min(verticsMin.z, v.position.z);
				verticsMax.z = max(verticsMax.z, v.position.z);
			}
			else if (str == "e")
			{
				int a, b;
				fp >> a >> b;

				Spring s;
				s.pointIDX[0] = a - 1;
				s.pointIDX[1] = b - 1;

				edges.push_back(s);
			}
			else if (str == "f")
			{
				int a, b, c;
				fp >> a >> b >> c;

				Mesh f;
				f.pointIDX[0] = a - 1;
				f.pointIDX[1] = b - 1;
				f.pointIDX[2] = c - 1;
				faces.push_back(f);
			}
			else
			{
				continue;
			}

		}

		verticsCom = (verticsMin + verticsMax) / 2.0f;

		fp.close();
	}

	void LoadAnimatedMesh(int frame, string path, string filename, float scale, Vector trans)
	{
		string zeros, fullname, filenum;

		if (frame < 10)
			zeros = "00000";
		else if (frame < 100)
			zeros = "0000";
		else if (frame < 1000)
			zeros = "000";
		else if (frame < 10000)
			zeros = "00";
		else if (frame < 100000)
			zeros = "0";

		filenum = to_string(frame);
		fullname = path + filename + zeros + filenum + ".obj";

		LoadMesh(fullname, scale, trans.x, trans.y, trans.z);
	}
};

