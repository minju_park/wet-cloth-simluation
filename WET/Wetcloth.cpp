#include "Wetcloth.h"

#define index(i,j) ((i)+(width+1)*(j))
#define Gidx(i,j,k) (((i)*res*res)+(j)*res+(k))

Wetcloth::Wetcloth()
{
	floor = -8;
}


Wetcloth::~Wetcloth()
{
}

void Wetcloth::SetClothParam(string ver, float w, float h, float timestep, float deformation, float damp, float stiff, float mass)
{
	version = ver;

	width = w;
	height = h;

	dt = timestep;
	torque = deformation;
	damping = damp;
	stiffness = stiff;

	pointmass = mass;
}

void Wetcloth::SetWindParam(float viscos, Vector direction)
{
	viscosity = viscos;
	windy = direction;
}

void Wetcloth::SetPullParam(float magnitude, float maximum, Vector direction)
{
	pullingMag = magnitude;
	pullingMax = maximum;
	pullingDir = direction;
}

void Wetcloth::SetFrictionParam(float MAXstatic, float kinetic)
{
	MaxStaticFriction = MAXstatic;
	KineticFritcion = kinetic;
}

void Wetcloth::ClearData()
{
	point.clear();
	spring.clear();
	mesh.clear();
	
	if (loadobj)
	{
		/* clear OBJ data */
		OBJpoint.clear();
		OBJspring.clear();
		OBJmesh.clear();
	}

}





/******* Initialize *******/

void Wetcloth::Initialize()
{
	if (loadcloth)
	{
		/*if you want to initialize the cloth as basic quad*/
		if (!initLoad)
		{
			InitPoints();
			InitSprings();
			InitMeshes();
		}

		/*if you want to initialize the cloth as imported .obj data*/
		if (initLoad)
			InitClothFromObj();

		FindClothLinkedSpring();
		FindClothLinkedPoint();
		FindClothLinkedMesh();
		FindClothAdjacentMesh();
		UpdateMeshNormal();
		UpdatePointNormal();
	}

	
	
	
	
	if (loadobj)
	{
		initOBJ();
		FindObjLinkedSpring();
		FindObjLinkedPoint();
		FindObjLinkedMesh();
		FindObjAdjacentMesh();
		UpdateObjMeshNormal();
		UpdateObjPointNormal();
	}


	if (loadobj) floor = (int) OBJ.verticsMin.y;

	deltatime = 0;

	BallPos = { 0.0f, 0.0f, 0.0f };
}


/** Initialize cloth **/

void Wetcloth::InitClothFromObj()
{
	/* init point */
	for (int i = 0; i < CLOTH.vertics.size(); i++)
	{
		point.push_back(CLOTH.vertics[i]);
		point[i].mass = pointmass;
		point[i].idx = i;
		if (point[i].position.norm() < 0.1f)point[i].fixed = true;
	}

	/* init spring */
	for (int j = 0; j < CLOTH.edges.size(); j++)
	{
		Spring s;

		s.p1 = &point[CLOTH.edges[j].pointIDX[0]];
		s.p2 = &point[CLOTH.edges[j].pointIDX[1]];

		s.initial_length = (s.p1->position - s.p2->position).norm();

		spring.push_back(s);
	}

	/* init mesh */
	for (int k = 0; k < CLOTH.faces.size(); k++)
	{
		Mesh m;
		
		m.p1 = &point[CLOTH.faces[k].pointIDX[0]];
		m.p2 = &point[CLOTH.faces[k].pointIDX[1]];
		m.p3 = &point[CLOTH.faces[k].pointIDX[2]];

		mesh.push_back(m);
	}

	for (int k = 0; k < mesh.size(); k++)
		for (int j = 0; j < spring.size(); j++)
		{
			if (((spring[j].p1 == mesh[k].p1) && (spring[j].p2 == mesh[k].p2))
				|| ((spring[j].p1 == mesh[k].p2) && (spring[j].p2 == mesh[k].p1)))
				mesh[k].s1 = &spring[j];

			if (((spring[j].p1 == mesh[k].p2) && (spring[j].p2 == mesh[k].p3))
				|| ((spring[j].p1 == mesh[k].p3) && (spring[j].p2 == mesh[k].p2)))
				mesh[k].s2 = &spring[j];

			if (((spring[j].p1 == mesh[k].p1) && (spring[j].p2 == mesh[k].p3))
				|| ((spring[j].p1 == mesh[k].p3) && (spring[j].p2 == mesh[k].p1)))
				mesh[k].s3 = &spring[j];
		}
}

void Wetcloth::InitPoints()
{
	int IDX = 0;

	int wid_start = 0 - width / 2;
	int wid_end = width - width / 2;
	int hei_start = 0 - height / 2;
	int hei_end = height - height / 2;


	for (int h = hei_start; h <= hei_end; h++)
		for (int w = wid_start; w <= wid_end; w++)
		{
			Point p;

			p.idx = IDX;
			p.acceleration = { 0.0f, 0.0f, 0.0f};
			p.pre_velo = { 0.0f, 0.0f, 0.0f};
			p.velocity = { 0.0f, -0.0f, 0.0f};

			p.mass = pointmass;

			p.force = { 0.0f, 0.0f, 0.0f};

			p.normal = { 0.0f, 0.0f, 0.0f};

			float yy;
			
			if (loadobj) yy = OBJ.verticsMax.y + 0.5f;
			else if (ball) yy = BallRadi+0.5f;
			else yy = 10.0f;

			if (version == "hang")
			{
				p.position = { (float)w, yy, (float)h };
			}
			else if (version == "drop")
			{
				p.position = { (float)w, yy, (float)h };

				if (w == (wid_start+wid_end) / 2 && h == (hei_start+hei_end) / 2)
					p.pulling = true;

			}
			else if (version == "flag")
			{
				p.position = { (float)w, (float)h, 0.0f };

				if (w == wid_start && ( h == hei_start || h == hei_end))
					p.fixed = true;
			}
			else if (version == "spin")
			{
				p.position = { (float) (w - wid_end/2.0f), 2.5f, (float) (h - hei_end/2.0f) };
			}
			else if (version == "collision")
			{
				p.position = { (float)w, (float)h, 0.0f };

				wind = true;
			}

			if (p.position.y <=floor)
				p.contactFloor = true;

			p.prepre_pos = p.position;
			p.pre_pos = p.position;

			point.push_back(p);

			IDX++;
		}
}

void Wetcloth::InitSprings()
{
	for (int h = 0; h <= (int)height; h++)
		for (int w = 0; w <= (int)width; w++)
		{
			Spring s;

			s.p1 = &point[index(w, h)];

			if (w != width)
			{
				s.p2 = &point[index(w + 1, h)];
				s.initial_length = s.length = s.p1->position.length(s.p2->position);
				s.type = "structural";
				spring.push_back(s);
			}

			if (h != height)
			{
				s.p2 = &point[index(w, h + 1)];
				s.initial_length = s.length = s.p1->position.length(s.p2->position);
				s.type = "structural";
				spring.push_back(s);
			}

			if (w < width - 1)
			{
				s.p2 = &point[index(w + 2, h)];
				s.initial_length = s.length = s.p1->position.length(s.p2->position);
				s.type = "flexion";
				spring.push_back(s);
			}

			if (h < height - 1)
			{
				s.p2 = &point[index(w, h + 2)];
				s.initial_length = s.length = s.p1->position.length(s.p2->position);
				s.type = "flexion";
				spring.push_back(s);
			}

			if (w != width && h != height)
			{
				s.p2 = &point[index(w + 1, h + 1)];
				s.initial_length = s.length = s.p1->position.length(s.p2->position);
				s.type = "shear";
				spring.push_back(s);
			}

			if (w != width && h != height)
			{
				s.p1 = &point[index(w + 1, h)];
				s.p2 = &point[index(w, h + 1)];
				s.initial_length = s.length = s.p1->position.length(s.p2->position);
				s.type = "shear";
				spring.push_back(s);
			}
		}
}

void Wetcloth::InitMeshes()
{
	/* init mesh by point */
	for (int h = 0; h < height; h++)
	{
		for (int w = 0; w < width; w++)
		{
			Mesh m;

			m.idx[0] = w + 1;
			m.idx[1] = h + 1;
			
			m.indx = index(w, h);

			/* overlap */
			m.p1 = &point[index(w, h)];
			m.p2 = &point[index(w + 1, h)];
			m.p3 = &point[index(w + 1, h + 1)];
			mesh.push_back(m);

			m.p2 = &point[index(w + 1, h)];
			m.p3 = &point[index(w, h + 1)];
			mesh.push_back(m);

			m.p2 = &point[index(w + 1, h + 1)];
			m.p3 = &point[index(w, h + 1)];
			mesh.push_back(m);

			m.p1 = &point[index(w, h + 1)];
			m.p2 = &point[index(w + 1, h)];
			m.p3 = &point[index(w + 1, h + 1)];
			mesh.push_back(m);
		}
	}

	/* init springs of mesh */
	for (int k = 0; k < mesh.size(); k++)
		for (int j = 0; j < spring.size(); j++)
		{
			if (spring[j].type == "flexion") continue;

			if (((spring[j].p1 == mesh[k].p1) && (spring[j].p2 == mesh[k].p2))
				|| ((spring[j].p1 == mesh[k].p2) && (spring[j].p2 == mesh[k].p1)))
				mesh[k].s1 = &spring[j];

			if (((spring[j].p1 == mesh[k].p2) && (spring[j].p2 == mesh[k].p3))
				|| ((spring[j].p1 == mesh[k].p3) && (spring[j].p2 == mesh[k].p2)))
				mesh[k].s2 = &spring[j];

			if (((spring[j].p1 == mesh[k].p1) && (spring[j].p2 == mesh[k].p3))
				|| ((spring[j].p1 == mesh[k].p3) && (spring[j].p2 == mesh[k].p1)))
				mesh[k].s3 = &spring[j];
		}

	/* init area */
	for (int k = 0; k < mesh.size(); k++)
	{
		float a = mesh[k].s1->length;
		float b = mesh[k].s2->length;
		float c = mesh[k].s3->length;
		float s = (a + b + c) / 2.0f;

		mesh[k].area = sqrt(s*(s - a)*(s - b)*(s - c));
	}
}

void Wetcloth::FindClothLinkedSpring()
{
	for (int j = 0; j < spring.size(); j++)
	{
		spring[j].p1->linked_spring.push_back(&spring[j]);
		spring[j].p2->linked_spring.push_back(&spring[j]);
	}
}

void Wetcloth::FindClothLinkedPoint()
{
	for (int i = 0; i < point.size(); i++)
		for (int j = 0; j < point[i].linked_spring.size(); j++)
		{
			if (point[i].linked_spring[j]->type == "flexion") continue;

			if (point[i].linked_spring[j]->p1 == &point[i])
				point[i].linked_point.push_back(point[i].linked_spring[j]->p2);
			else
				point[i].linked_point.push_back(point[i].linked_spring[j]->p1);
		}
}

void Wetcloth::FindClothLinkedMesh()
{
	for (int k = 0; k < mesh.size(); k++)
	{
		mesh[k].p1->linked_mesh.push_back(&mesh[k]);
		mesh[k].p2->linked_mesh.push_back(&mesh[k]);
		mesh[k].p3->linked_mesh.push_back(&mesh[k]);
	}
}

void Wetcloth::FindClothAdjacentMesh()
{
	for (int j = 0; j < spring.size(); j++)
		for (int k = 0; k < mesh.size(); k++)
		{
			if (&spring[j] == mesh[k].s1)
			{
				if (spring[j].m1 == NULL)
					spring[j].m1 = &mesh[k];
				else
					spring[j].m2 = &mesh[k];
			}
			else if (&spring[j] == mesh[k].s2)
			{
				if (spring[j].m1 == NULL)
					spring[j].m1 = &mesh[k];
				else
					spring[j].m2 = &mesh[k];
			}
			else if (&spring[j] == mesh[k].s3)
			{
				if (spring[j].m1 == NULL)
					spring[j].m1 = &mesh[k];
				else
					spring[j].m2 = &mesh[k];
			}
		}
}



/** Initialize collider object **/

void Wetcloth::initOBJ()
{
	OBJpoint.clear();
	OBJspring.clear();
	OBJmesh.clear();


	/* init OBJ points */
	for (int i = 0; i < OBJ.vertics.size(); i++)
		OBJpoint.push_back(OBJ.vertics[i]);

	/* init OBJ springs */
	for (int j = 0; j < OBJ.edges.size(); j++)
	{
		Spring s;

		s.p1 = &OBJpoint[OBJ.edges[j].pointIDX[0]];
		s.p2 = &OBJpoint[OBJ.edges[j].pointIDX[1]];

		s.initial_length = (s.p1->position - s.p2->position).norm();

		OBJspring.push_back(s);
	}

	/* init OBJ meshes */
	for (int k = 0; k < OBJ.faces.size(); k++)
	{
		Mesh m;

		m.p1 = &OBJpoint[OBJ.faces[k].pointIDX[0]];
		m.p2 = &OBJpoint[OBJ.faces[k].pointIDX[1]];
		m.p3 = &OBJpoint[OBJ.faces[k].pointIDX[2]];

		OBJmesh.push_back(m);
	}
}

void Wetcloth::FindObjLinkedSpring()
{
	for (int j = 0; j < OBJspring.size(); j++)
	{
		OBJspring[j].p1->linked_spring.push_back(&OBJspring[j]);
		OBJspring[j].p2->linked_spring.push_back(&OBJspring[j]);
	}
}

void Wetcloth::FindObjLinkedPoint()
{
	for (int i = 0; i < OBJpoint.size(); i++)
		for (int j = 0; j < OBJpoint[i].linked_spring.size(); j++)
		{
			if (OBJpoint[i].linked_spring[j]->p1 == &OBJpoint[i])
				OBJpoint[i].linked_point.push_back(OBJpoint[i].linked_spring[j]->p2);
			else
				OBJpoint[i].linked_point.push_back(OBJpoint[i].linked_spring[j]->p1);
		}
}

void Wetcloth::FindObjLinkedMesh()
{
	for (int k = 0; k < OBJmesh.size(); k++)
	{
		OBJmesh[k].p1->linked_mesh.push_back(&OBJmesh[k]);
		OBJmesh[k].p2->linked_mesh.push_back(&OBJmesh[k]);
		OBJmesh[k].p3->linked_mesh.push_back(&OBJmesh[k]);
	}
}

void Wetcloth::FindObjAdjacentMesh()
{
	for (int j = 0; j < OBJspring.size(); j++)
		for (int k = 0; k < OBJmesh.size(); k++)
		{
			if (&OBJspring[j] == OBJmesh[k].s1)
			{
				if (OBJspring[j].m1 == NULL)
					OBJspring[j].m1 = &OBJmesh[k];
				else
					OBJspring[j].m2 = &OBJmesh[k];
			}
			else if (&OBJspring[j] == OBJmesh[k].s2)
			{
				if (OBJspring[j].m1 == NULL)
					OBJspring[j].m1 = &OBJmesh[k];
				else
					OBJspring[j].m2 = &OBJmesh[k];
			}
			else if (&OBJspring[j] == OBJmesh[k].s3)
			{
				if (OBJspring[j].m1 == NULL)
					OBJspring[j].m1 = &OBJmesh[k];
				else
					OBJspring[j].m2 = &OBJmesh[k];
			}
		}
}


/******* Draw *******/

void Wetcloth::Draw()
{
	if (drawPoint)			DrawPoints();
	if (drawSpring)			DrawSprings();
	if (drawMesh)			DrawMesh();
	if (drawMeshNormal)		DrawMeshNormal();
	if (drawPointNormal)	DrawPointNormal();
	if (drawVelocity)		DrawVelocity();
	if (drawForce)			DrawForce();
	if (drawObjvelo)		DrawObjVelo();
}

void Wetcloth::DrawCollider()
{
	if (offsetmesh)
	{
		glBegin(GL_TRIANGLES);
		for (int k = 0; k < OBJmesh.size(); k++)
		{
			glNormal3f(OBJmesh[k].normal.x, OBJmesh[k].normal.y, OBJmesh[k].normal.z);

			glNormal3f(OBJmesh[k].p1->normal.x, OBJmesh[k].p1->normal.y, OBJmesh[k].p1->normal.z);
			glVertex3f(OBJmesh[k].p1->position.x, OBJmesh[k].p1->position.y, OBJmesh[k].p1->position.z);
			glNormal3f(OBJmesh[k].p2->normal.x, OBJmesh[k].p2->normal.y, OBJmesh[k].p2->normal.z);
			glVertex3f(OBJmesh[k].p2->position.x, OBJmesh[k].p2->position.y, OBJmesh[k].p2->position.z);
			glNormal3f(OBJmesh[k].p3->normal.x, OBJmesh[k].p3->normal.y, OBJmesh[k].p3->normal.z);
			glVertex3f(OBJmesh[k].p3->position.x, OBJmesh[k].p3->position.y, OBJmesh[k].p3->position.z);
		}
		glEnd();
	}


	if (!offsetmesh)
	{
		glBegin(GL_TRIANGLES);
		for (int k = 0; k < OBJmesh.size(); k++)
		{
			glNormal3f(OBJmesh[k].normal.x, OBJmesh[k].normal.y, OBJmesh[k].normal.z);

			Vector of1 = OBJmesh[k].p1->position - 0.7f*OBJmesh[k].p1->normal;
			Vector of2 = OBJmesh[k].p2->position - 0.7f*OBJmesh[k].p2->normal;
			Vector of3 = OBJmesh[k].p3->position - 0.7f*OBJmesh[k].p3->normal;

			glNormal3f(OBJmesh[k].p1->normal.x, OBJmesh[k].p1->normal.y, OBJmesh[k].p1->normal.z);
			glVertex3f(of1.x, of1.y, of1.z);
			glNormal3f(OBJmesh[k].p2->normal.x, OBJmesh[k].p2->normal.y, OBJmesh[k].p2->normal.z);
			glVertex3f(of2.x, of2.y, of2.z);
			glNormal3f(OBJmesh[k].p3->normal.x, OBJmesh[k].p3->normal.y, OBJmesh[k].p3->normal.z);
			glVertex3f(of3.x, of3.y, of3.z);
		}
		glEnd();
	}
}

void Wetcloth::DrawPoints()
{
	glDisable(GL_LIGHTING);

	if (drawPoint)
	{
		glPointSize(10.0f);
		glBegin(GL_POINTS);
		for (int i = 0; i < point.size(); i++)
		{
			if (point[i].fixed||point[i].pulling) glColor3f(0.8f, 0.3f, 0.7f);
			else glColor3f(1.0f, 1.0f, 1.0f);

			/* contact test */
			if (contactCheck)
			{
				if (point[i].contactBall || point[i].contactFloor|| point[i].contactObj||point[i].contactSelf)
					glColor3f(0.0f, 0.0f, 1.0f);
				else 
					glColor3f(1.0f, 1.0f, 1.0f);
			}

			glVertex3f(point[i].position.x, point[i].position.y, point[i].position.z);
		}
		glEnd();
	}

	glEnable(GL_LIGHTING);
}

void Wetcloth::DrawSprings()
{
	glDisable(GL_LIGHTING);
	
	glColor3f(0.0f, 0.0f, 0.0f);

	glBegin(GL_LINES);
	for (int j = 0; j < spring.size(); j++)
	{
		if (spring[j].type == "flexion") continue;

		float	max_len = spring[j].initial_length*(1.0f + torque),
			min_len = spring[j].initial_length*(1.0f - torque);

		if (spring[j].length > max_len) glColor3f(0.0f, 1.0f, 1.0f);
		else if (spring[j].length < min_len) glColor3f(1.0f, 0.0f, 1.0f);
		else glColor3f(0.0f, 0.0f, 0.0f);

		glVertex3f(spring[j].p1->position.x, spring[j].p1->position.y, spring[j].p1->position.z);
		glVertex3f(spring[j].p2->position.x, spring[j].p2->position.y, spring[j].p2->position.z);
	}
	glEnd();

	glEnable(GL_LIGHTING);
}

void Wetcloth::DrawMesh()
{
	glColor3f(0.5f, 0.5f, 0.5f);

	glBegin(GL_TRIANGLES);
	for (int k = 0; k < mesh.size(); k++)
	{
		glNormal3f(mesh[k].normal.x, mesh[k].normal.y, mesh[k].normal.z);

		glNormal3f(mesh[k].p1->normal.x, mesh[k].p1->normal.y, mesh[k].p1->normal.z);
		glVertex3f(mesh[k].p1->position.x, mesh[k].p1->position.y, mesh[k].p1->position.z);
		glNormal3f(mesh[k].p2->normal.x, mesh[k].p2->normal.y, mesh[k].p2->normal.z);
		glVertex3f(mesh[k].p2->position.x, mesh[k].p2->position.y, mesh[k].p2->position.z);
		glNormal3f(mesh[k].p3->normal.x, mesh[k].p3->normal.y, mesh[k].p3->normal.z);
		glVertex3f(mesh[k].p3->position.x, mesh[k].p3->position.y, mesh[k].p3->position.z);
	}
	glEnd();
}

void Wetcloth::DrawMeshNormal()
{
	glDisable(GL_LIGHTING);

	glColor3f(0.5f, 1.0f, 0.5f);

	glBegin(GL_LINES);
	for (int k = 0; k < mesh.size(); k++)
	{
		Vector com = (mesh[k].p1->position + mesh[k].p2->position + mesh[k].p3->position) / 3.0f;
		Vector normal = com + mesh[k].normal;

		glVertex3f(com.x, com.y, com.z);
		glVertex3f(normal.x, normal.y, normal.z);
	}
	glEnd();

	glEnable(GL_LIGHTING);
}

void Wetcloth::DrawPointNormal()
{
	glDisable(GL_LIGHTING);

	glColor3f(1.0f, 0.5f, 0.5f);


	glBegin(GL_LINES);
	for (int i = 0; i < point.size(); i++)
	{
		glVertex3f(point[i].position.x, point[i].position.y, point[i].position.z);
		glVertex3f(point[i].position.x + point[i].normal.x, point[i].position.y + point[i].normal.y, point[i].position.z + point[i].normal.z);
	}
	glEnd();

	glEnable(GL_LIGHTING);
}

void Wetcloth::DrawVelocity()
{
	glDisable(GL_LIGHTING);

	glColor3f(1.0f, 1.0f, 0.0f);

	glBegin(GL_LINES);
	for (int i = 0; i < point.size(); i++)
	{
		glVertex3f(point[i].position.x, point[i].position.y, point[i].position.z);
		glVertex3f(point[i].position.x + point[i].velocity.normalize().x / 2.0f,
			point[i].position.y + point[i].velocity.normalize().y / 2.0f,
			point[i].position.z + point[i].velocity.normalize().z / 2.0f);
	}
	glEnd();

	glEnable(GL_LIGHTING);
}

void Wetcloth::DrawForce()
{
	glDisable(GL_LIGHTING);

	glColor3f(1.0f, 0.0f, 0.0f);

	glBegin(GL_LINES);
	for (int i = 0; i < point.size(); i++)
	{
		glVertex3f(point[i].position.x, point[i].position.y, point[i].position.z);
		glVertex3f(point[i].position.x + point[i].force_adhesion.normalize().x / 2.0f,
			point[i].position.y + point[i].force_adhesion.normalize().y / 2.0f,
			point[i].position.z + point[i].force_adhesion.normalize().z / 2.0f);
	}
	glEnd();

	glEnable(GL_LIGHTING);
}

void Wetcloth::DrawBall()
{
	glDisable(GL_LIGHTING);

	glColor3f(0.5f, 0.5f, 0.5f);

	glPushMatrix();
	glTranslatef(BallPos.x, BallPos.y, BallPos.z);
	glutSolidSphere(BallRadi-0.1f, 30, 30);
	glPopMatrix();

	glEnable(GL_LIGHTING);
}

void Wetcloth::DrawObjVelo()
{
	glDisable(GL_LIGHTING);

	glColor3f(1.0f, 1.0f, 0.0f);

	glBegin(GL_LINES);
	for (int i = 0; i < OBJpoint.size(); i++)
	{
		glVertex3f(OBJpoint[i].position.x, OBJpoint[i].position.y, OBJpoint[i].position.z);
		glVertex3f(OBJpoint[i].position.x + OBJpoint[i].velocity.normalize().x / 2.0f,
			OBJpoint[i].position.y + OBJpoint[i].velocity.normalize().y / 2.0f,
			OBJpoint[i].position.z + OBJpoint[i].velocity.normalize().z / 2.0f);
	}
	glEnd();

	glEnable(GL_LIGHTING);
}





/******* Calculate force & position *******/

void Wetcloth::TotalForce(bool drag, bool ex, bool in, bool friction, bool adhesion)
{
	if (drag) DragForce();
	if (ex) ExternalForce();
	if (in) InternalForce();
	PullingForce();
	if (adhesion) AdhesionForce();
	if (friction) FrictionForce();

	Vector exsum = { 0, 0, 0 }, insum = { 0, 0, 0 };
	
	for (int i = 0; i < point.size(); i++)
	{
		point[i].force = point[i].force_adhesion 
						+ point[i].force_airpress 
						+ point[i].force_external 
						+ point[i].force_internal 
						+ point[i].force_pull
						+ point[i].force_friction;

		exsum += point[i].force_external;
		insum += point[i].force_internal;
		
		if (point[i].fixed) point[i].force = { 0.0f, 0.0f, 0.0f};
	}

}

void Wetcloth::PullingForce()
{
	for (int i = 0; i < point.size(); i++)
	{
		if (pulling)
		{
			if (point[i].pulling)
			{
				if (point[i].force_pull.norm() < pullingMax)
					point[i].force_pull += pullingMag*pullingDir;

				pullF = point[i].force_pull.norm();
			}
		}
		else
			point[i].force_pull = { 0.0f, 0.0f, 0.0f };
	}


}

void Wetcloth::ExternalForce()
{
	for (int i = 0; i < point.size(); i++)
	{
		point[i].force_external = Gravity(point[i]) + Dissipation(point[i]) + Viscosity(point[i]); 
	}
}

Vector Wetcloth::Gravity(Point p)
{
	return p.mass*gravity;
}

Vector Wetcloth::Dissipation(Point p)
{
	return -1.0f*damping*p.velocity;
}

Vector Wetcloth::Viscosity(Point p)
{
	if (wind == true)
		return viscosity*p.normal*(windy - p.velocity)*p.normal;
	else
		return { 0.0f, 0.0f, 0.0f };	
}

void Wetcloth::InternalForce()
{
	for (int i = 0; i < point.size(); i++)
	{
	
		Vector tension, damp;
		tension = { 0.0f, 0.0f, 0.0f}; damp = { 0.0f, 0.0f, 0.0f};

		for (int j = 0; j < point[i].linked_spring.size(); j++)
		{

			Vector l_dir;
			float l_init = point[i].linked_spring[j]->initial_length;

			if (point[i].linked_spring[j]->p1 == &point[i])
			{
				l_dir = point[i].linked_spring[j]->p2->position - point[i].position;
				damp -= 1.0f*(point[i].velocity - point[i].linked_spring[j]->p2->velocity);
			}
			else
			{
				l_dir = point[i].linked_spring[j]->p1->position - point[i].position;
				damp -= 1.0f*(point[i].velocity - point[i].linked_spring[j]->p1->velocity);
			}

			tension += stiffness*(l_dir.norm() - l_init)*l_dir.normalize();
		}
	
		point[i].force_internal = tension + damp;
	}


	/* old (from Mass-spring system) */
	//for (int j = 0; j < spring.size(); j++)
	//{
	//	Vector l = spring[j].p2->position - spring[j].p1->position;
	//	float l0 = spring[j].initial_length;
	//	Vector dl = l - l0*(l / l.norm());
	//
	//	spring[j].p1->force += stiffness*dl;
	//	spring[j].p2->force -= stiffness*dl;
	//
	//	if (spring[j].p1->fixed == true)
	//		spring[j].p2->force -= 5.0f*stiffness*dl;
	//	if (spring[j].p2->fixed == true)
	//		spring[j].p1->force += 5.0f*stiffness*dl;
	//}
}

void Wetcloth::FrictionForce()
{
	/* control with velocity */
	for (int i = 0; i < point.size(); i++)
		if (point[i].contactBall || point[i].contactFloor || point[i].contactObj)
		{
			Vector vn, vt, pre_vt;
			
			vn = (point[i].ContactNormal*point[i].velocity)*point[i].ContactNormal;
			pre_vt = (point[i].ContactNormal*point[i].velocity)*point[i].ContactNormal;

			Vector Fn = (point[i].ContactNormal*point[i].force)*point[i].ContactNormal.normalize();


			float dvn=(Fn*dt/point[i].mass).norm();
			float mu = 1.0f;
			float k = 1.0f - (mu*dvn / pre_vt.norm());

			if (k > 0)
				vt = k*pre_vt.normalize();
			else
				vt = 0.0f*pre_vt.normalize();

			point[i].velocity = vn + vt;
		}
}

void Wetcloth::AdhesionForce()
{
	for (int i = 0; i < point.size(); i++)
	{
		if (point[i].contactBall || point[i].contactFloor || point[i].contactObj||point[i].contactSelf)
		{
			/* surface tension of liquid bridge */
			float Ra = 0.5f; // radi of liquid bridge
			float gamma = 72.75f / 5.0f;

			/* h must be the length between point[i].position and its contact mesh[k].position */
			float h;

			if (point[i].contactFloor)
				h = point[i].position.length(Vector(point[i].position.x, floor, point[i].position.z));
			else if (point[i].contactObj)
			{
				Vector AP = point[i].position - point[i].ContactMesh->p1->position;
				h = AP*point[i].ContactNormal;
			}
			else if (point[i].contactSelf)
			{
				h = 0.1f;
			}
			else if (point[i].contactBall)
			{
				h = (point[i].position - BallPos).norm() - BallRadi + 0.1;
			}


			if (h < 0.00000000001f) h = h + 0.1f;

			float theta_a = (1.0f - h)*(pi / 2.0f);

			Vector f = gamma*pi*Ra*sin(theta_a)*(-1.0f*point[i].ContactNormal);


			/* preesure difference between atmosphere and liquid bridge */
			float theta_b = theta_a;

			f += (gamma*pi*Ra*Ra)*(cos(theta_a) + cos(theta_b))*(-1.0f*point[i].ContactNormal) / h;

			point[i].force_adhesion = 1.0f*f;
		}
		else
			point[i].force_adhesion = { 0.0f, 0.0f, 0.0f };
	}
}

void Wetcloth::DragForce()
{
	for (int i = 0; i < point.size(); i++)
	{
		float air_density = 1.225; /* unit = kg/m^3 */
		float drag_coeff = 0.5f; /* for triangle shape */

		Vector f = air_density*drag_coeff*point[i].velocity.norm()*point[i].velocity.norm()*Vector(0.0f,0.0f,-0.1f) / 2.0f;

		point[i].force = f;
	}
}



void Wetcloth::SpringConstraint()
{
	for (int j = 0; j < spring.size(); j++)
	{
		if (spring[j].type == "flexion") continue;

		Vector direction = spring[j].p2->position - spring[j].p1->position;
		float current_length = direction.norm();
		float deformation = (current_length - spring[j].initial_length) / current_length;

		if (spring[j].p1->fixed && spring[j].p2->fixed) continue;

		if (!spring[j].p1->fixed) spring[j].p1->force -= 10.0f*deformation*direction;
		if (!spring[j].p2->fixed) spring[j].p2->force += 10.0f*deformation*direction;
	}
}

void Wetcloth::springconstraint2()
{
	int itermax = 100; // pulling scene = 1, self-collision scene = 10, other scenes = 200

	for (int iter = 0; iter < itermax; iter++)
	{
		for (int j = 0; j < spring.size(); j++)
		{
			if (spring[j].type == "flexion") continue;

			Vector delta = spring[j].p2->position - spring[j].p1->position;
			float len = delta.norm();
			float diff = (len - spring[j].initial_length) / len;

			if (spring[j].p1->fixed && spring[j].p2->fixed) continue;

			if (!spring[j].p1->fixed) spring[j].p1->position += delta*0.5f*diff;
			else spring[j].p2->position -= delta*0.5f*diff;

			if (!spring[j].p2->fixed) spring[j].p2->position -= delta*0.5f*diff;
			else spring[j].p1->position += delta*0.5f*diff;
		}
	}
}

void Wetcloth::Accelation()
{
	for (int i = 0; i < point.size(); i++)
	{
		point[i].contactObj = false;
		point[i].contactSelf = false;

		point[i].acceleration = point[i].force / point[i].mass;
	}
}

void Wetcloth::Velocity()
{
	for (int i = 0; i < point.size(); i++)
	{
		point[i].pre_velo = point[i].velocity;
		point[i].velocity += dt*point[i].acceleration;
	}
}

void Wetcloth::Position(string type)
{
	for (int i = 0; i < point.size(); i++)
	{
		point[i].prepre_pos = point[i].pre_pos;
		point[i].pre_pos = point[i].position;


		if (!point[i].fixed)
		{
			if (type == "euler")
				point[i].position += dt*point[i].velocity;
			else if (type == "midpoint")
				point[i].position += dt*point[i].velocity + 0.5f*dt*dt*point[i].acceleration;
			else if (type == "verlet")
				point[i].position += 0.0001f*(point[i].position - point[i].pre_pos) + point[i].acceleration*dt*dt;
		}
	}
}



/******* Object velocity & position update *******/

void Wetcloth::ObjVelocity()
{
	for (int i = 0; i < OBJpoint.size(); i++)
	{
		Vector dir = OBJpoint[i].position - OBJpoint[i].pre_pos;

		if (dir.norm() > 0.0001f)
		{
			OBJpoint[i].velocity = (dir.norm() / dt)*dir.normalize();
		}
		else
		{
			OBJpoint[i].velocity = { 0, 0, 0 };
		}
	}
}


void Wetcloth::ObjPosition()
{
	if (loadanimation)
		for (int i = 0; i < OBJ.vertics.size(); i++)
		{
			OBJpoint[i].pre_pos = OBJpoint[i].position;
			OBJpoint[i].position = OBJ.vertics[i].position;
		}
	else
		for (int i = 0; i < OBJpoint.size(); i++)
		{
			OBJpoint[i].pre_pos = OBJpoint[i].position;
		
			float step = 0.03f;

			if (BallRotate)
				OBJpoint[i].position = { OBJpoint[i].position.x*cos(step) - OBJpoint[i].position.z*sin(step),
				OBJpoint[i].position.y, OBJpoint[i].position.x*sin(step) + OBJpoint[i].position.z*cos(step) };
		}
}







/******* Collision detcetion *******/

bool Wetcloth::rayTriangleIntersect(Vector orig, Vector dir, Vector v0, Vector v1, Vector v2, float &t)
{
	const float EPSILON = 0.001;
	Vector vertex0 = v0;
	Vector vertex1 = v1;
	Vector vertex2 = v2;
	Vector edge1, edge2, h, s, q;
	float a, f, u, v;
	edge1 = vertex1 - vertex0;
	edge2 = vertex2 - vertex0;
	h = dir.cross(edge2);
	a = edge1*(h);
	if (a > -EPSILON && a < EPSILON)
		return false;
	f = 1 / a;
	s = orig - vertex0;
	u = f * (s*(h));
	if (u < 0.0 || u > 1.0)
		return false;
	q = s.cross(edge1);
	v = f * dir*(q);
	if (v < 0.0 || u + v > 1.0)
		return false;
	// At this stage we can compute t to find out where the intersection point is on the line.
	t = f * edge2*(q);
	if (t > EPSILON) // ray intersection
	{
		return true;
	}
	else // This means that there is a line intersection but not a ray intersection.
		return false;
}

bool Wetcloth::TriPointDetection(Mesh *t, Point *p)
{
	/* use current & previous position */
	Vector AP, APpre;
	AP = p->position - t->p1->position;
	APpre = p->pre_pos - t->p1->position;

	if ((AP*t->normal)*(APpre*t->normal) < 0)
		return true;
	else
		return false;
}

bool Wetcloth::EdgeEdgeDetection(Spring *s1, Spring *s2)
{
	Vector AB = s1->p1->position - s1->p2->position;
	Vector CD = s2->p1->position - s2->p2->position;
	Vector AC = s2->p2->position - s1->p2->position;

	if ((AB.cross(CD))*AC <= 0.00001f)
		return true;
	else
		return false;
}

void Wetcloth::TriPointResponse(Mesh *t, Point *p)
{
	if (TriPointDetection(t, p))
	{
		p->position = p->pre_pos;
		p->pre_pos = p->prepre_pos;
	}
}

void Wetcloth::EdgeEdgeReponse(Spring *s1, Spring *s2)
{
	if (EdgeEdgeDetection(s1, s2))
	{
		s1->p1->position = s1->p1->pre_pos;
		s1->p1->pre_pos = s1->p1->prepre_pos;
		s1->p2->position = s1->p2->pre_pos;
		s1->p2->pre_pos = s1->p2->prepre_pos;

		s1->p1->position = s1->p1->pre_pos;
		s1->p1->pre_pos = s1->p1->prepre_pos;
		s1->p2->position = s1->p2->pre_pos;
		s1->p2->pre_pos = s1->p2->prepre_pos;
	}
}

void Wetcloth::SelfCollisionDetection()
{
	/* new method */
	bool hit_tri = false;
	float push = 0.1f;

	for (int k = 0; k < mesh.size(); k++)
	{
		Vector com = (mesh[k].p1->position + mesh[k].p2->position + mesh[k].p3->position) / 3.0f;

		if (com.y > (CLOTH.verticsMax.y + CLOTH.verticsMin.y) / 2) continue;

		for (int i = 0; i < point.size(); i++)
		{

			if (point[i].position.y > (CLOTH.verticsMax.y + CLOTH.verticsMin.y) / 2) continue;

			if (mesh[k].p1->idx == i || mesh[k].p2->idx == i || mesh[k].p3->idx == i) continue;

			if (point[i].fixed) continue;

			

			if ((com - point[i].position).norm() <0.9f)
			{
				Vector dir = point[i].pre_pos - point[i].position;
				float t;

				hit_tri = rayTriangleIntersect(point[i].position, dir.normalize(),
					mesh[k].p1->position, mesh[k].p2->position, mesh[k].p3->position, t);

				Vector AP = point[i].position - com;
				Vector APpre = point[i].pre_pos - com;


				if ((AP*mesh[k].normal) >= 0 && (APpre*mesh[k].normal) <= 0)
				{

					if (!point[i].fixed)
					{

						point[i].velocity -= (point[i].velocity*mesh[k].normal)*mesh[k].normal;
						point[i].velocity *= 0.5f;
						point[i].position -= (abs(AP*mesh[k].normal) + push)*mesh[k].normal;

						point[i].ContactNormal = -1.0f*mesh[k].normal;
						point[i].ContactMesh = &mesh[k];

						point[i].contactSelf = true;
					}

					if (!mesh[k].p1->fixed)
					{
						mesh[k].p1->velocity -= (mesh[k].p1->velocity*point[i].normal)*point[i].normal;
						mesh[k].p1->velocity *= 0.5f;
						mesh[k].p1->position += (0.5f*abs(AP*mesh[k].normal) + push)*mesh[k].normal;

						mesh[k].p1->ContactNormal = mesh[k].normal;

						mesh[k].p1->contactSelf = true;
					}

					if (!mesh[k].p2->fixed)
					{
						mesh[k].p2->velocity -= (mesh[k].p2->velocity*point[i].normal)*point[i].normal;
						mesh[k].p2->velocity *= 0.5f;
						mesh[k].p2->position += (0.5f*abs(AP*mesh[k].normal) + push)*mesh[k].normal;

						mesh[k].p2->ContactNormal = mesh[k].normal;

						mesh[k].p2->contactSelf = true;
					}

					if (!mesh[k].p3->fixed)
					{
						mesh[k].p3->velocity -= (mesh[k].p3->velocity*point[i].normal)*point[i].normal;
						mesh[k].p3->velocity *= 0.5f;
						mesh[k].p3->position += (0.5f*abs(AP*mesh[k].normal) + push)*mesh[k].normal;

						mesh[k].p2->ContactNormal = mesh[k].normal;

						mesh[k].p3->contactSelf = true;
					}
				}
			}
		}
	}
}

void Wetcloth::ObjectCollisionDetection()
{
	/* point-triangle */
	bool hit_tri = false;
	
	for (int i = 0; i < point.size(); i++)
		for (int k = 0; k < OBJmesh.size(); k++)
		{
			Vector com = (OBJmesh[k].p1->position 
				+ OBJmesh[k].p2->position + OBJmesh[k].p3->position) / 3.0f;
	
			float t;

			Vector meanVelo = (OBJmesh[k].p1->velocity + OBJmesh[k].p2->velocity + OBJmesh[k].p3->velocity) / 3.0f;
	
			if ((com - point[i].position).norm() < 10.0f) /* if close */
			{
				Vector N = OBJmesh[k].normal;
	
				Vector dir = point[i].pre_pos - point[i].position;
	
				hit_tri = rayTriangleIntersect(point[i].position, dir.normalize(),
					OBJmesh[k].p1->position, OBJmesh[k].p2->position,
					OBJmesh[k].p3->position, t);
	
				Vector AP = point[i].position - OBJmesh[k].p1->position;
	
				if (hit_tri && AP*N < 0)
				{
					if (point[i].fixed) continue;
	
					
					Vector vn, vt;
					vn = (point[i].velocity*(-1.0f*N))*(-1.0f*N);
					vt = point[i].velocity - vn;
					point[i].velocity = .9f*vt - 0.1f*vn;

					if (meanVelo.norm() > 0.00001f)
						point[i].velocity += meanVelo/2.0f;

					point[i].position -= (N*(-1.0f*dir))*N;
					point[i].pre_pos = point[i].position;
	
					point[i].ContactNormal = N;
					point[i].ContactMesh = &OBJmesh[k];
					point[i].contactObj = true;
				}
			}
		}
}




/******* Ball collider control *******/

void Wetcloth::BallCollision(Vector pos, float radi)
{

	for (int i = 0; i < point.size(); i++)
	{
		Vector gap = point[i].position - pos;
		Vector n   = gap.normalize();

		if (gap.norm() < radi)
		{
			point[i].contactBall = true;
			point[i].ContactNormal = n;
			point[i].velocity -= (n*point[i].velocity)*n; // velocity reflection
			point[i].velocity = 0.5f*point[i].velocity;
			point[i].position += (radi - gap.norm())*gap.normalize();
			point[i].pre_pos = point[i].position;
		}
		else
			point[i].contactBall = false;
	}
}

void Wetcloth::BallMove(string direction)
{
	float step = 0.1f;

	if (direction == "left")
	{
		if (ball) BallPos += {-step, 0.0f, 0.0f};

		if (loadobj)
			for (int i = 0; i < OBJpoint.size(); i++)
			{
				OBJpoint[i].pre_pos = OBJpoint[i].position;
				OBJpoint[i].position.x += -step;
			}
	}
	else if (direction == "right")
	{
		if (ball) BallPos += {+step, 0.0f, 0.0f};

		if (loadobj)
			for (int i = 0; i < OBJpoint.size(); i++)
			{
				OBJpoint[i].pre_pos = OBJpoint[i].position;
				OBJpoint[i].position.x += step;
			}
	}
	else if (direction == "up")
	{
		if (ball) BallPos += {0.0f, +step, 0.0f};

		if (loadobj)
			for (int i = 0; i < OBJpoint.size(); i++)
			{
				OBJpoint[i].pre_pos = OBJpoint[i].position;
				OBJpoint[i].position.y += step;
			}
	}
	else if (direction == "down")
	{
		if (ball) BallPos += {0.0f, -step, 0.0f};

		if (loadobj)
			for (int i = 0; i < OBJpoint.size(); i++)
			{
				OBJpoint[i].pre_pos = OBJpoint[i].position;
				OBJpoint[i].position.y += -step;
			}
	}
	else if (direction == "front")
	{
		if (ball) BallPos += {0.0f, 0.0f, +step};

		if (loadobj)
			for (int i = 0; i < OBJpoint.size(); i++)
			{
				OBJpoint[i].pre_pos = OBJpoint[i].position;
				OBJpoint[i].position.z += step;
			}
	}
	else if (direction == "back")
	{
		if (ball) BallPos += {0.0f, 0.0f, -step};

		if (loadobj)
			for (int i = 0; i < OBJpoint.size(); i++)
			{
				OBJpoint[i].pre_pos = OBJpoint[i].position;
				OBJpoint[i].position.z += -step;
			}
	}
}



/******* Update data *******/

void Wetcloth::UpdateClothData()
{
	UpdateMeshNormal();
	UpdatePointNormal();
	UpdateSpringLength();
}

void Wetcloth::UpdateMeshNormal()
{
	for (int k = 0; k < mesh.size(); k++)
	{
		Vector p1p2, p1p3, n;
	
		p1p2 = mesh[k].p2->position - mesh[k].p1->position;
		p1p3 = mesh[k].p3->position - mesh[k].p1->position;

		n = p1p2.cross(p1p3);
		n = n.normalize();
	
		if (!initLoad) n = -1.0f*n;

		mesh[k].normal = n;

		mesh[k].BB(); // update bounding box of a primitive
	}
}

void Wetcloth::UpdatePointNormal()
{
	for (int i = 0; i < point.size(); i++)
	{
		Vector sum; sum = { 0.0f, 0.0f, 0.0f};

		for (int l = 0; l < point[i].linked_mesh.size(); l++)
		{
			sum += point[i].linked_mesh[l]->normal;
		}

		point[i].normal = sum / point[i].linked_mesh.size();
	}
}

void Wetcloth::UpdatePointMass()
{
	for (int i = point.size()/2; i < point.size(); i++)
	{
		float delta = 0.5f;
		float theta = 0.84f; // from [Physical simulation of wet clothing for virtual human]

		point[i].mass += (delta / theta)*exp(-1.0f*deltatime / theta);
	}

	deltatime += dt;
}

void Wetcloth::UpdateSpringLength()
{
	for (int j = 0; j < spring.size(); j++)
		spring[j].length = (spring[j].p1->position - spring[j].p2->position).norm();
}


void Wetcloth::UpdateObjData()
{
	/* if load animation */
	
	//for (int i = 0; i < OBJpoint.size(); i++)
	//	OBJpoint[i].position.z += 0.01f;

	UpdateObjMeshNormal();
	UpdateObjPointNormal();
}

void Wetcloth::UpdateObjMeshNormal()
{
	for (int k = 0; k < OBJmesh.size(); k++)
	{
		Vector p1p2, p1p3, n;

		p1p2 = OBJmesh[k].p2->position - OBJmesh[k].p1->position;
		p1p3 = OBJmesh[k].p3->position - OBJmesh[k].p1->position;

		n = p1p2.cross(p1p3);
		n = n.normalize();

		OBJmesh[k].normal = n;
	}
}

void Wetcloth::UpdateObjPointNormal()
{
	for (int i = 0; i < OBJpoint.size(); i++)
	{
		Vector sum = { 0.0f, 0.0f, 0.0f };
		
		for (int k = 0; k < OBJpoint[i].linked_mesh.size(); k++)
		{
			sum += OBJpoint[i].linked_mesh[k]->normal;
		}

		OBJpoint[i].normal = sum / OBJpoint[i].linked_mesh.size();
	}
}





/******* Close floor *******/

void Wetcloth::CloseTheBottom()
{
	for (int i = 0; i < point.size(); i++)
		if (point[i].position.y < floor)
		{
			point[i].contactFloor = true;
			point[i].ContactNormal = { 0.0f, 1.0f, 0.0f };
			//point[i].velocity -= (Vector{ 0.0f, 1.0f, 0.0f }*point[i].velocity)* Vector{ 0.0f, 1.0f, 0.0f };
			//point[i].velocity = 0.1f*point[i].velocity;
			point[i].velocity = { 0, 0, 0 };
			point[i].position.y = floor;
		}
		else
			point[i].contactFloor = false;
}





/*********** Export cloth mesh **********/
void Wetcloth::ExportClothMesh(int frame, string path, string exportingname)
{
	string zeros, fullname, filenum;

	if (frame < 10)
		zeros = "00000";
	else if (frame < 100)
		zeros = "0000";
	else if (frame < 1000)
		zeros = "000";
	else if (frame < 10000)
		zeros = "00";
	else if (frame < 100000)
		zeros = "0";

	filenum = to_string(frame);

	fullname = path + exportingname + zeros + filenum + ".obj";

	FILE *fp;
	char temp[100];
	sprintf_s(temp, fullname.c_str());
	fopen_s(&fp, temp, "w");

	for (int i = 0; i < point.size(); i++)
		fprintf(fp, "v %f %f %f\n", point[i].position.x, point[i].position.y, point[i].position.z);

	for (int k = 0; k < mesh.size(); k++)
		fprintf(fp, "f %d %d %d\n", mesh[k].p3->idx+1, mesh[k].p2->idx+1, mesh[k].p1->idx+1);

	fclose(fp);
}