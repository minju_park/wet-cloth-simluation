#pragma once
#include <math.h>
class Vector
{
public:
	float x;
	float y;
	float z;

public:
	Vector() : x(0), y(0), z(0) {}

	Vector(const float x, const float y, const float z)
		:x(x), y(y), z(z){}

	Vector operator+(const Vector v) const
	{
		return Vector(x + v.x, y + v.y, z + v.z);
	}
	Vector& operator+=(const Vector v)
	{
		x += v.x;
		y += v.y;
		z += v.z;

		return *this;
	}


	Vector operator-(const Vector v) const
	{
		return Vector(x - v.x, y - v.y, z - v.z);
	}
	Vector& operator-=(const Vector v)
	{
		x -= v.x;
		y -= v.y;
		z -= v.z;

		return *this;
	}

	float operator*(const Vector v) const
	{
		return x*v.x + y*v.y + z*v.z;
	}
	Vector operator*(float k) const
	{
		return Vector(k*x, k*y, k*z);
	}
	friend Vector operator*(float k, Vector v)
	{
		return Vector(k*v.x, k*v.y, k*v.z);
	}

	Vector operator/(float k) const
	{
		return Vector(x / k, y / k, z / k);
	}

	bool operator==(const Vector v) const
	{
		if ((v.x == x) && (v.y == y) && (v.z == z))
			return true;
		else
			return false;
	}

	bool operator!=(const Vector v) const
	{
		if ((v.x == x) && (v.y == y) && (v.z == z))
			return false;
		else
			return true;
	}

	Vector operator *=(float k) const
	{
		return Vector(x*k, y*k, z*k );
	}


	float norm()
	{
		return sqrt(x*x + y*y + z*z);
	}

	Vector normalize()
	{
		return{ x / norm(), y / norm(), z / norm() };
	}

	Vector cross(Vector v)
	{
		return{ y * v.z - z * v.y, z * v.x - x * v.z, x * v.y - y * v.x };
	}

	bool isEqual(Vector v)
	{
		if (x == v.x && y == v.y && z == v.z) return true;
		else return false;
	}

	void equal(float k)
	{
		x = k;
		y = k;
		z = k;
	}

	void make0()
	{
		x = 0.0f;
		y = 0.0f;
		z = 0.0f;
	}

	float length(Vector v)
	{
		return sqrt((x - v.x)*(x - v.x) + (y - v.y)*(y - v.y) + (z - v.z)*(z - v.z));
	}

	~Vector(){};
};