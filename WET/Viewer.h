#pragma once
#include <Windows.h>
#include <gl/GL.h>
#include <gl/GLU.h>
#include <gl/glut.h>
#include "Simulator.h"

class Viewer
{
public:
	Viewer();
	~Viewer();

public:
	Simulator simulator;

public:
	void Initialize();
	void Update();
	void Render();
	void Keyborad(unsigned char key, int x, int y);
	void SpecialKey(int key, int x, int y);
	void Mouse(int mouse_event, int state, int x, int y);
	void Motion(int x, int y);
	void Reshape(int w, int h);
	void Lighting(Vector color);

	void DrawTXT(const char  *text, int length, float x, float y);
	double FPScounter();
	double FPS;

public:
	bool working = false;
	bool movingswitch = true;

public:
	// mouse data (m)
	int mX, mY;
	unsigned char mEventLeft, mEventMiddle, mEventRight;
	// rotation(r) & translation(t) & zooming(z) data
	float rX, rY, rZ;
	float tX, tY;
	float zZ;

private:
	double deltaTime;
};

