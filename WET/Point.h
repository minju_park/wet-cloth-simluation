#pragma once
#include "Vector.h"
#include <vector>

using namespace std;

class Spring;
class Mesh;
class Point
{
public:
	Point(){};
	~Point(){};

public:
	std::vector<Spring*> linked_spring;
	std::vector<Point*> linked_point;
	std::vector<Mesh*> linked_mesh;


public:
	int		idx;
	float	mass;
	bool	fixed	= false,
			pulling = false;
	bool	contactFloor = false,
			contactBall = false,
			contactObj = false,
			contactSelf = false;
	float	radius	= 0.5f;
	float	volume	= 4.0f*3.14159265359f*pow(radius, 3) / 3;
	int		hashx, hashy, hashz;


public:
	Vector	acceleration;
	
	Vector	pre_velo;
	Vector	velocity;
	
	Vector	prepre_pos;
	Vector	pre_pos;
	Vector	position;
	
	Vector	force;
		Vector	force_adhesion;
		Vector	force_external;
		Vector	force_internal;
		Vector	force_airpress;
		Vector	force_pull;
		Vector	force_friction;
	
	Vector	normal;

	Vector ContactNormal;
	Mesh *ContactMesh;
};

