#include <stdio.h>
#include <stdlib.h>
#include <gl/GL.H>
#include <gl/GLU.H>
#include <gl/glut.h>
#include "Viewer.h"
#include "FileManager.h"

Viewer viewer;
FileManager capture;
int w = 1500;
int h = 800;

void Init()
{
	viewer.Initialize();
}

void Render()
{
	viewer.Render();
}

void Idle()
{
	if (viewer.working)
	{
		viewer.Update();
		//capture.ScreenCapture(w,h);
	}
}

void Reshape(int w, int h)
{
	viewer.Reshape(w, h);
}

void Keyboard(unsigned char key, int x, int y)
{
	viewer.Keyborad(key, x, y);
}

void SpecialKeyboard(int key, int x, int y)
{
	viewer.SpecialKey(key, x, y);
}

void Mouse(int mouse_event, int state, int x, int y)
{
	viewer.Mouse(mouse_event, state, x, y);
}

void Motion(int x, int y)
{
	viewer.Motion(x, y);
}

int main(int argc, char* argv[])
{
	Init();
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGBA | GLUT_DEPTH | GLUT_DOUBLE);
	//glutInitWindowPosition(2000, 0);
	//glutInitWindowSize(h, w);
	glutInitWindowPosition(250, 100);
	glutInitWindowSize(w, h);
	glutCreateWindow("Wet Cloth Simulation by MINJU");
	glutDisplayFunc(Render);
	glutReshapeFunc(Reshape);
	glutMouseFunc(Mouse);
	glutMotionFunc(Motion);
	glutKeyboardFunc(Keyboard);
	glutSpecialFunc(SpecialKeyboard);
	glutIdleFunc(Idle);


	glutMainLoop();

	return 0;
}