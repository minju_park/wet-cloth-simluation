#include "Simulator.h"


Simulator::Simulator()
{
	movingstep = 0.1f;
	compare = 0;
}


Simulator::~Simulator()
{
	wet.ClearData();

	if (compare) comparison.ClearData();
}

void Simulator::SetVeiw(Vector eye, Vector lookat)
{
	eyes = eye;
	view = lookat;
}


void Simulator::init()
{
	frame = 0;

	Vector pull = { 0.0f, 0.5f, 0.0f };

	wet.ClearData();
	if (wet.loadobj) wet.OBJ.LoadMesh("ObjFile/e_sphere352.obj", 5.0f, 0.0f, 0.0f, 0.0f);
	if (wet.initLoad) wet.CLOTH.LoadMesh("ObjFile/e_quad.obj", 3.0f, 0.0f, 0.0f, 0.0f);
	wet.SetClothParam("drop", 60.0f, 60.0f, 0.02f, 0.001f, 0.9f, 100.0f, 1.0f);
	wet.SetPullParam(10.0f, 3000.0f, pull);
	wet.SetWindParam(70.0f, { 5.0f, -5.0f, -5.0f });
	wet.SetFrictionParam(10.0f, 10.0f);
	wet.Initialize();


	SetVeiw({ 0.0f, 50.0f, 50.0f }, { 0.0f, 25.0f, 0.0f });


	if (compare)
	{
		comparison.ClearData();
		if (comparison.loadobj)	comparison.OBJ.LoadMesh("ObjFile/e_staticpepsi4569.obj", 15.0f, 0.0f, 0.0f, 0.0f);
		if (comparison.initLoad) comparison.CLOTH.LoadMesh("ObjFile/e_staticpepsi_smooth_flare3077_fixedpoints.obj", 15.0f, 0.0f, 0.0f, 0.0f);
		comparison.SetClothParam("hang", 20.0f, 40.0f, 0.02f, 0.001f, 0.9f, 700.0f, 2.0f);
		comparison.SetPullParam(10.0f, 3000.0f, pull);
		comparison.SetWindParam(70.0f, { 5.0f, -5.0f, -5.0f });
		comparison.SetFrictionParam(10.0f, 10.0f);
		comparison.Initialize();
	}
}

void Simulator::update()
{
	if (wet.loadanimation)
		wet.OBJ.LoadAnimatedMesh(frame, "ObjFile/forwardball/", "sphere_", 5.0f, { 0.0f, 0.0f, -30.0f });
	wet.UpdateClothData();
	if (wet.loadobj) wet.UpdateObjData();
	wet.TotalForce(false, true, true, false, true);
	wet.Accelation();
	if (wet.loadobj) wet.ObjVelocity();
	wet.Velocity(); 
	wet.FrictionForce();
	if (wet.loadobj) wet.ObjPosition();
	wet.Position("euler");
	wet.springconstraint2();
	if (wet.loadobj) wet.ObjectCollisionDetection();
	wet.SelfCollisionDetection();
	if (wet.ball) wet.BallCollision(wet.BallPos, wet.BallRadi);
	if (wet.exporting) wet.ExportClothMesh(frame, "Scene/Obj/nonadhesion_", "windy_");
	wet.CloseTheBottom();


	if (compare)
	{
		if (comparison.loadanimation)
			comparison.OBJ.LoadAnimatedMesh(frame, "ObjFile/forwardball/", "sphere_", 5.0f, { 0.0f, 0.0f, -30.0f });
		comparison.UpdateClothData();
		if (comparison.loadobj) comparison.UpdateObjData();
		comparison.TotalForce(false, true, true, false, true);
		comparison.Accelation();
		if (comparison.loadobj) comparison.ObjVelocity();
		comparison.Velocity();
		comparison.FrictionForce();
		if (comparison.loadobj) comparison.ObjPosition();
		comparison.Position("euler");
		comparison.springconstraint2();
		if (comparison.loadobj) comparison.ObjectCollisionDetection();
		comparison.SelfCollisionDetection();
		if (comparison.ball) comparison.BallCollision(comparison.BallPos, comparison.BallRadi);
		if (comparison.exporting) comparison.ExportClothMesh(frame, "Scene/Obj/adhesion_", "windy_");
		comparison.CloseTheBottom();
	}
	
	
	frame++;
}


void Simulator::render()
{
	wet.Draw();
	if (wet.ball) wet.DrawBall();

	if (compare)
	{
		glPushMatrix();
		glTranslatef(m*wet.width, 0.0f, 0.0f);
		comparison.Draw();
		if (comparison.ball) comparison.DrawBall();
		glTranslatef(-m*wet.width, 0.0f, 0.0f);
		glPopMatrix();
	}

	DrawFloor();
}


void Simulator::DrawAxis()
{
	glDisable(GL_LIGHTING);

	glBegin(GL_LINES);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 0.0f, 0.0f);
	glVertex3f(1.0f, 0.0f, 0.0f);

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);

	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 0.0f, 1.0f);

	glEnd();

	glEnable(GL_LIGHTING);
}


void Simulator::DrawFloor()
{
	glDisable(GL_LIGHTING);
	
	glColor3f(0.8f, 0.8f, 0.8f);

	glBegin(GL_QUADS);
	glNormal3f(0.0f, 1.0f, 0.0f);
	glVertex3f(-5000.0f, wet.floor-0.15f, -5000.0f);
	glVertex3f(+5000.0f, wet.floor-0.15f, -5000.0f);
	glVertex3f(+5000.0f, wet.floor-0.15f, +5000.0f);
	glVertex3f(-5000.0f, wet.floor-0.15f, +5000.0f);
	glEnd();

	glEnable(GL_LIGHTING);
}

void Simulator::move(string moving)
{
	if (moving == "up")
	{
		for (int i = 0; i < wet.point.size(); i++)
		{
			if (wet.point[i].fixed == true)
				wet.point[i].position.y += movingstep;
		}

		for (int i = 0; i < comparison.point.size(); i++)
		{
			if (comparison.point[i].fixed == true)
				comparison.point[i].position.y += movingstep;
		}
	}

	if (moving == "down")
	{
		for (int i = 0; i < wet.point.size(); i++)
		{
			if (wet.point[i].fixed == true)
				wet.point[i].position.y -= movingstep;
		}

		for (int i = 0; i < comparison.point.size(); i++)
		{
			if (comparison.point[i].fixed == true)
				comparison.point[i].position.y -= movingstep;
		}
	}

	if (moving == "left")
	{
		for (int i = 0; i < wet.point.size(); i++)
		{
			if (wet.point[i].fixed == true)
				wet.point[i].position.x -= movingstep;
		}

		for (int i = 0; i < comparison.point.size(); i++)
		{
			if (comparison.point[i].fixed == true)
				comparison.point[i].position.x -= movingstep;
		}
	}

	if (moving == "right")
	{
		for (int i = 0; i < wet.point.size(); i++)
		{
			if (wet.point[i].fixed == true)
				wet.point[i].position.x += movingstep;
		}

		for (int i = 0; i < comparison.point.size(); i++)
		{
			if (comparison.point[i].fixed == true)
				comparison.point[i].position.x += movingstep;
		}
	}

	if (moving == "front")
	{
		for (int i = 0; i < wet.point.size(); i++)
		{
			if (wet.point[i].fixed == true)
				wet.point[i].position.z += movingstep;
		}

		for (int i = 0; i < comparison.point.size(); i++)
		{
			if (comparison.point[i].fixed == true)
				comparison.point[i].position.z += movingstep;
		}
	}

	if (moving == "back")
	{
		for (int i = 0; i < wet.point.size(); i++)
		{
			if (wet.point[i].fixed == true)
				wet.point[i].position.z -= movingstep;
		}

		for (int i = 0; i < comparison.point.size(); i++)
		{
			if (comparison.point[i].fixed == true)
				comparison.point[i].position.z -= movingstep;
		}
	}

}

void Simulator::SimulatioInfo()
{
	cout << "[SIMULATION INFO]" << endl << endl;
	cout << " Cloth point size : " << wet.point.size() << endl;
	cout << " Cloth spring size : " << wet.spring.size() << endl;
	cout << " Cloth mesh size : " << wet.mesh.size() << endl;
	cout << " Object point size : " << wet.OBJpoint.size() << endl;
	cout << " Object spring size : " << wet.OBJspring.size() << endl;
	cout << " Object mesh size : " << wet.OBJmesh.size() << endl << endl << endl;
}

void Simulator::PrintParameterInfo()
{
	cout << "[SETTING PARAMETERS]" << endl << endl;
	cout << " torque = "			<< wet.torque		<< endl;
	cout << " dt = "				<< wet.dt			<< endl;
	cout << " damping = "			<< wet.damping		<< endl;
	cout << " viscosity = "			<< wet.viscosity	<< endl;
	cout << " stiffness = "			<< wet.stiffness	<< endl;
	cout << " windy = "				<< "{" << wet.windy.x << ", " << wet.windy.y << ", " << wet.windy.z << "}" << endl;
	cout << " pullingDir = "		<< "{" << wet.pullingDir.x << ", " << wet.pullingDir.y << ", " << wet.pullingDir.z << "}" << endl;
	cout << " pullingMag = "		<< wet.pullingMag << endl;
	cout << " pullingMax = "		<< wet.pullingMax << endl ;
	cout << " MaxStaticFriction = " << wet.MaxStaticFriction << endl;
	cout << " KineticFritcion = "	<< wet.KineticFritcion << endl ;
	cout << " floor = "				<< wet.floor << endl << endl << endl;
}

void Simulator::PrintKeyInfo()
{
	cout << "[SETTING KEYS]" << endl << endl;
	cout << " a		" << endl;
	cout << " b		" << endl;
	cout << " c		draw contract points" << endl;
	cout << " d		" << endl;
	cout << " e		draw springs" << endl;
	cout << " f		draw force" << endl;
	cout << " g		add pull force" << endl;
	cout << " h		" << endl;
	cout << " i		rotate view +z" << endl;
	cout << " j		" << endl;
	cout << " k		" << endl;
	cout << " l		clear window" << endl;
	cout << " m		draw meshes" << endl;
	cout << " n		" << endl;
	cout << " o		draw offset mesh" << endl;
	cout << " p		draw points" << endl;
	cout << " q		exit" << endl;
	cout << " r		replay" << endl;
	cout << " s		switch moving object/cloth" << endl;
	cout << " t		draw face normal" << endl;
	cout << " u		rotate view -z" << endl;
	cout << " v		draw velocity" << endl;
	cout << " w		windy" << endl;
	cout << " x		draw vertex normal" << endl;
	cout << " y		draw object velocity" << endl;
	cout << " z		draw collider" << endl;
	cout << " q/Q/esc	exit" << endl;
	cout << " space		play/pause" << endl;
	cout << " ,		moving back" << endl;
	cout << " .		moving front" << endl;
	cout << " ?		print simulation info" << endl;
	cout << " !		print key info" << endl << endl;

}