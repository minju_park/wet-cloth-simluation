#include "Viewer.h"

Viewer::Viewer()
{
	mX = mY = 0;
	tX = tY = 0;
	zZ = 0;
	mEventLeft = 0;
	mEventMiddle = 0;
	mEventRight = 0;
}


Viewer::~Viewer()
{
}


void Viewer::Initialize()
{
	glEnable(GL_DEPTH_TEST);
	glClearColor(1, 1, 1, 1.0f);

	simulator.init();
	simulator.update();
}

void Viewer::Update()
{
	simulator.update();
	FPS = FPScounter();
}

void Viewer::Render()
{
	glEnable(GL_DEPTH_TEST);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(1, 1, 1, 1.0f);
	glMatrixMode(GL_MODELVIEW);

	glLoadIdentity();


	gluLookAt(simulator.eyes.x, simulator.eyes.y, simulator.eyes.z, 
			  simulator.view.x, simulator.view.y, simulator.view.z, 
			  0.0f, 1.0f, 0.0f);

	glTranslatef(0.0f, 0.0f, zZ);
	glTranslatef(tX, -tY, 0.0f);
	glRotatef(rX, 0.0f, 1.0f, 0.0f);
	glRotatef(rY, 1.0f, 0.0f, 0.0f);
	glRotatef(rZ, 0.0f, 0.0f, 1.0f);


	//simulator.DrawAxis();
	
	//Lighting(Vector(0.99, 0.78, 0.05));	// yellow
	Lighting(Vector(0.94, 0.12, 0.12)); // red
	//Lighting(Vector(0.72, 0.66, 0.96)); // purple
	//Lighting(Vector(0.44, 0.57, 0.75));  // blue    cloth color
	simulator.wet.Draw();

	//Lighting(Vector(0.73, 0.0, 0.0));

	if (simulator.compare)
	{
		glPushMatrix();
		glTranslatef(simulator.m*simulator.wet.width, 0.0f, 0.0f);
		simulator.comparison.Draw();
		glTranslatef(-simulator.m*simulator.wet.width, 0.0f, 0.0f);
		glPopMatrix();
	}

	simulator.DrawFloor();
	glDisable(GL_LIGHTING);



	Lighting(Vector(0.8f, 0.8f, 0.8f)); // light gray obj color
	if (simulator.wet.drawCollider) simulator.wet.DrawCollider();
	if (simulator.wet.ball) simulator.wet.DrawBall();

	if (simulator.compare && simulator.comparison.drawCollider)
	{
		glTranslatef(simulator.m*simulator.wet.width, 0.0f, 0.0f);
		simulator.comparison.DrawCollider();
		glTranslatef(-simulator.m*simulator.wet.width, 0.0f, 0.0f);
	}

 
	glDisable(GL_LIGHTING);
	glColor3f(.1, .1, .1);
	string s;
	s = "Frame : " + to_string(simulator.frame);
	DrawTXT(s.data(), s.size(), -.9, .9);

	string s2;
	s2 = "FPS : " + to_string(FPS);
	DrawTXT(s2.data(), s2.size(), -.9, .87);

	string s3;
	s3 = "pulling force : " + to_string((int)simulator.wet.pullF) + "N";
	DrawTXT(s3.data(), s3.size(), -.9, .84);



	glutSwapBuffers();
	glutPostRedisplay();
	
}

void Viewer::Lighting(Vector color)
{
	glShadeModel(GL_SMOOTH);			 // Set Flat Shading
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
//	float light_pos[] = { 50.0, 25.0, 50.0, 0.0 };
	float light_dir[] = { 0, 0, 0 };
	float light_ambient[] = { 0.1, 0.1, 0.1, 1.0 };
	float light_diffuse[] = { 0.8, 0.8, 0.8, 1.0 };
	float light_specular[] = { 1.0, 1.0, 1.0, 1.0 };
	float frontColor[] = { color.x, color.y, color.z, 1 };
	//	float frontColor[] = { 0.8, 0.8, 0.8, 1 };
	float matShininess = 100;
	float noMat[] = { 0, 0, 0, 1 };
	float matSpec[] = { 1.0, 1.0, 1.0, 1.0 };
	glMaterialfv(GL_FRONT, GL_EMISSION, noMat);
	//glMaterialfv(GL_FRONT, GL_SPECULAR, matSpec);
	glMaterialfv(GL_FRONT, GL_AMBIENT, frontColor);		// Set ambient property frontColor.
	glMaterialfv(GL_FRONT, GL_DIFFUSE, frontColor);		// Set diffuse property frontColor.
	glMaterialf(GL_FRONT, GL_SHININESS, matShininess);
	glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
	//glLightfv(GL_LIGHT0, GL_POSITION, light_pos);
	glLightfv(GL_LIGHT0, GL_SPOT_DIRECTION, light_dir);
	glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
	//	glDisable(GL_LIGHTING);
}

void Viewer::Reshape(int w, int h)
{
	float scal = 1.3f;

	if (w == 0)	h = 1;
	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(60, (float)w / h, 0.1, 500);
	//glOrtho(-45 * scal, 180 * scal, -15 * scal, 105 * scal, -1000, 1000);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

void Viewer::Keyborad(unsigned char key, int x, int y)
{
	switch (key)
	{
	case 'q':
	case 'Q':
	case 27:
		exit(0);
		break;
	case 'r':
		system("cls");
		cout << " REPLAY" << endl;
		Initialize();
		break;
	case 32:
		working = !working;
		break;
	case 'w':
		simulator.wet.wind = !simulator.wet.wind;
		simulator.comparison.wind = !simulator.comparison.wind;
		if (simulator.wet.wind) cout << " Wind ON " << endl;
		else cout << " Wind OFF " << endl;
		break;
	case 'p':
		simulator.wet.drawPoint = !simulator.wet.drawPoint;
		simulator.comparison.drawPoint = !simulator.comparison.drawPoint;
		if (simulator.wet.drawPoint) cout << " Draw points ON " << endl;
		else cout << " Draw points OFF " << endl;
		break;
	case 'e':
		simulator.wet.drawSpring = !simulator.wet.drawSpring;
		simulator.comparison.drawSpring = !simulator.comparison.drawSpring;
		if (simulator.wet.drawSpring) cout << " Draw springs ON " << endl;
		else cout << " Draw springs OFF " << endl;
		break;
	case 'm':
		simulator.wet.drawMesh = !simulator.wet.drawMesh;
		simulator.comparison.drawMesh = !simulator.comparison.drawMesh;
		if (simulator.wet.drawMesh) cout << " Draw mesh ON " << endl;
		else cout << " Draw mesh OFF " << endl;
		break;
	case 't':
		simulator.wet.drawMeshNormal = !simulator.wet.drawMeshNormal;
		simulator.comparison.drawMeshNormal = !simulator.comparison.drawMeshNormal;
		if (simulator.wet.drawMeshNormal) cout << " Draw face normal ON " << endl;
		else cout << " Draw face normal OFF " << endl;
		break;
	case 'x':
		simulator.wet.drawPointNormal = !simulator.wet.drawPointNormal;
		simulator.comparison.drawPointNormal = !simulator.comparison.drawPointNormal;
		if (simulator.wet.drawPointNormal) cout << " Draw vertex normal ON " << endl;
		else cout << " Draw vertex normal OFF " << endl;
		break;
	case ',':
		if (movingswitch) simulator.move("back");
		else
		{
			simulator.wet.BallMove("back");
			simulator.comparison.BallMove("back");
		}
		break;
	case '.':
		if (movingswitch) simulator.move("front");
		else
		{
			simulator.wet.BallMove("front");
			simulator.comparison.BallMove("front");
		}
		break;
	case 'u':
		rZ -= 5.0f;
		break;
	case 'i':
		rZ += 5.0f;
		break;
	case 'c':
		simulator.wet.contactCheck = !simulator.wet.contactCheck;
		simulator.comparison.contactCheck = !simulator.comparison.contactCheck;
		if (simulator.wet.contactCheck) cout << " Draw contact points ON " << endl;
		else cout << " Draw contact points OFF " << endl;
		break;
	case 'g':
		simulator.wet.pulling = !simulator.wet.pulling;
		simulator.comparison.pulling = !simulator.comparison.pulling;
		if (simulator.wet.pulling) cout << " Pulling force ON " << endl;
		else cout << " Pulling force OFF " << endl;
		break;
	case 'v':
		simulator.wet.drawVelocity = !simulator.wet.drawVelocity;
		simulator.comparison.drawVelocity = !simulator.comparison.drawVelocity;
		if (simulator.wet.drawVelocity) cout << " Draw velocity ON " << endl;
		else cout << " Draw velocity OFF " << endl;
		break;
	case '?':
		simulator.SimulatioInfo();
		simulator.PrintParameterInfo();
		break;
	case '!':
		simulator.PrintKeyInfo();
		break;
	case 'f':
		simulator.wet.drawForce = !simulator.wet.drawForce;
		simulator.comparison.drawForce = !simulator.comparison.drawForce;
		if (simulator.wet.drawForce) cout << " Draw force ON " << endl;
		else cout << " Draw force OFF " << endl;
		break;
	case 's':
		movingswitch = !movingswitch;
		if (movingswitch) cout << " Moving mode : fixed points of cloth mesh" << endl;
		else cout << " Moving mode : object" << endl;
		break;
	case 'o':
		simulator.wet.offsetmesh = !simulator.wet.offsetmesh;
		simulator.comparison.offsetmesh = !simulator.comparison.offsetmesh;
		if (simulator.wet.offsetmesh) cout << " Draw offset mesh ON " << endl;
		else cout << " Draw offset mesh OFF " << endl;
		break;
	case 'z':
		simulator.wet.drawCollider = !simulator.wet.drawCollider;
		simulator.comparison.drawCollider = !simulator.comparison.drawCollider;
		if (simulator.wet.drawCollider) cout << " Draw collider ON " << endl;
		else cout << " Draw collider OFF " << endl;
		break;
	case 'l':
		system("cls");
		cout << " CLEAR" << endl;
		break;
	case 'y':
		simulator.wet.drawObjvelo = !simulator.wet.drawObjvelo;
		simulator.comparison.drawObjvelo = !simulator.comparison.drawObjvelo;
		if (simulator.wet.drawObjvelo) cout << " Draw object velocity ON " << endl;
		else cout << " Draw object velocity OFF " << endl;
		break;
	case '/':
		simulator.wet.BallRotate = !simulator.wet.BallRotate;
		simulator.comparison.BallRotate = !simulator.comparison.BallRotate;
		if (simulator.wet.BallRotate) cout << " Rotate ball ON " << endl;
		else cout << " Rotate ball OFF " << endl;
		break;
	default:
		break;
	}

	glutPostRedisplay();
}

void Viewer::SpecialKey(int key, int x, int y)
{
	switch (key)
	{
	case GLUT_KEY_UP:
		if (movingswitch) simulator.move("up");
		if (!movingswitch)
		{
			simulator.wet.BallMove("up");
			simulator.comparison.BallMove("up");
		}
		break;
	case GLUT_KEY_DOWN:
		if (movingswitch) simulator.move("down");
		if (!movingswitch)
		{
			simulator.wet.BallMove("down");
			simulator.comparison.BallMove("down");
		}
		break;
	case GLUT_KEY_LEFT:
		if (movingswitch) simulator.move("left");
		if (!movingswitch)
		{
			simulator.wet.BallMove("left");
			simulator.comparison.BallMove("left");
		}
		break;
	case GLUT_KEY_RIGHT:
		if (movingswitch) simulator.move("right");
		if (!movingswitch)
		{
			simulator.wet.BallMove("right");
			simulator.comparison.BallMove("right");
		}
		break;
	}

	glutPostRedisplay();

}

void Viewer::Mouse(int mouse_event, int state, int x, int y)
{
	mX = x;
	mY = y;

	switch (mouse_event)
	{
	case GLUT_LEFT_BUTTON:
		mEventLeft = ((GLUT_DOWN == state) ? 1 : 0);
		break;
	case GLUT_MIDDLE_BUTTON:
		mEventMiddle = ((GLUT_DOWN == state) ? 1 : 0);
		break;
	case GLUT_RIGHT_BUTTON:
		mEventRight = ((GLUT_DOWN == state) ? 1 : 0);
		break;
	default:
		break;
	}

	glutPostRedisplay();
}

void Viewer::Motion(int x, int y)
{
	int diffX = x - mX;
	int diffY = y - mY;

	mX = x;
	mY = y;

	if (mEventLeft)
	{
		rX += (float) 0.1f*diffX;
		rY += (float) 0.1f*diffY;
	}
	else if (mEventRight)
	{
		tX += (float) 0.05f*diffX;
		tY += (float) 0.05f*diffY;
	}
	else if (mEventMiddle)
	{
		zZ += (float) 0.1f*diffY;
	}
}

void Viewer::DrawTXT(const char  *text, int length, float x, float y)
{
	glMatrixMode(GL_PROJECTION);
	double *matrix = new double[16];
	glGetDoublev(GL_PROJECTION_MATRIX, matrix);
	glLoadIdentity();
	glOrtho(0, 800, 0, 600, -5, -5);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glPushMatrix();
	glLoadIdentity();
	glRasterPos2f(x, y);
	for (int i = 0; i < length; i++)
		glutBitmapCharacter(GLUT_BITMAP_9_BY_15, (int)text[i]);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glLoadMatrixd(matrix);
	glMatrixMode(GL_MODELVIEW);
}

double Viewer::FPScounter()
{
	static double previousTime = glutGet(GLUT_ELAPSED_TIME);
	double currentTime = glutGet(GLUT_ELAPSED_TIME);
	deltaTime = currentTime - previousTime;
	previousTime = currentTime;
	
	return (1000.0f/deltaTime);
}
