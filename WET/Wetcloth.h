#pragma once
#include "Point.h"
#include "Spring.h"
#include "Mesh.h"
#include "MeshLoader.h"
#include <vector>
#include <iostream>
#include <gl/GL.H>
#include <gl/GLU.H>
#include <gl/glut.h>

using namespace std;

class Wetcloth
{
public:
	Wetcloth();
	~Wetcloth();

public:
	vector<Point>	point;
	vector<Spring>	spring;
	vector<Mesh>	mesh;




public:
	vector<Point>	OBJpoint;
	vector<Spring>	OBJspring;
	vector<Mesh>	OBJmesh;



public:
	Vector	gravity = { 0.0f, -9.8f, 0.0f };
	float	pi = 3.14159265359f;



public:
	string	version;
	float	width,
			height,
			dt,
			torque,
			damping,
			stiffness, // of springs
			pointmass;


public:
	bool	wind = false;
	float	viscosity;
	Vector	windy;



public:
	bool	pulling = false;
	float	pullingMag;
	float	pullingMax;
	Vector	pullingDir;
	float	pullF;


public:
	float MaxStaticFriction;
	float KineticFritcion;



public:
	void SetClothParam(string ver, float w, float h, float timestep, float deformation, float damp, float stiff, float mass);
	void SetWindParam(float viscos, Vector direction);
	void SetPullParam(float magnitude, float maximum, Vector direction);
	void SetFrictionParam(float MAXstatic, float kinetic);



public:
	void Initialize();
		MeshLoader CLOTH;
			void InitClothFromObj();
			void InitPoints();
			void InitSprings();
			void InitMeshes();
			void FindClothLinkedSpring();
			void FindClothLinkedPoint();
			void FindClothLinkedMesh();
			void FindClothAdjacentMesh();
		MeshLoader OBJ;
			void initOBJ();
			void FindObjLinkedSpring();
			void FindObjLinkedPoint();
			void FindObjLinkedMesh();
			void FindObjAdjacentMesh();


public:
	bool drawPoint = false,
		 drawSpring = false,
		 drawMesh = true,
		 drawMeshNormal = false,
		 drawPointNormal = false,
		 drawVelocity = false,
		 drawForce = false,
		 drawCollider = true,
		 drawObjvelo = false;

	void Draw();
		void DrawPoints();
		void DrawSprings();
		void DrawMesh();
		void DrawMeshNormal();
		void DrawPointNormal();
		void DrawVelocity();
		void DrawForce();
		void DrawBall();
		void DrawCollider();
		void DrawObjVelo();


public:
	void ClearData();



public:
	void TotalForce(bool drag, bool ex, bool in, bool friction, bool adhesion);
		void ExternalForce();
			Vector Gravity(Point p);
			Vector Dissipation(Point p);
			Vector Viscosity(Point p);
		void InternalForce();
		void FrictionForce();
		void AdhesionForce();
		void DragForce();
		void PullingForce();
	void SpringConstraint();
	void springconstraint2();
	void Accelation();
	void Velocity();
	void Position(string type);

	void ObjVelocity();
	void ObjPosition();
	

public:
	bool rayTriangleIntersect(Vector orig, Vector dir, Vector v0, Vector v1, Vector v2, float &t);
	void SelfCollisionDetection();
		bool TriPointDetection(Mesh *t, Point *p);
		bool EdgeEdgeDetection(Spring *s1, Spring *s2);
		void TriPointResponse(Mesh *t, Point *p);
		void EdgeEdgeReponse(Spring *s1, Spring *s2);
	void ObjectCollisionDetection();


public:
	bool BallRotate = false;
	Vector BallPos;
	void BallCollision(Vector pos, float radi);
	void BallMove(string direction);



public:
	void UpdateClothData();
		void UpdateMeshNormal();
		void UpdatePointNormal();
		void UpdatePointMass();
		void UpdateSpringLength();
	void UpdateObjData();
		void UpdateObjMeshNormal();
		void UpdateObjPointNormal();

	float deltatime = 0;

public:
	bool contactCheck = false;
	int floor;
	void CloseTheBottom();




public:
	void ExportClothMesh(int frame, string path, string exportingname);


public:
	/* to control the scene */
	bool loadcloth		= 1; // cloth load or not
	bool initLoad		= 0; // initialize loaded cloth

	bool loadobj		= 0; // initialize loaded collider
	bool loadanimation	= 0; // load animated object

	bool offsetmesh = 0; // draw offset mesh

	bool ball		= 1; // draw ball collider in opengl
	float BallRadi	= 8.0f;

	bool exporting = 0;
};